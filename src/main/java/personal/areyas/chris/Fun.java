package personal.areyas.chris;

public class Fun {

  public static int addOne(int input) {
    return input + 1;
  }

  public static String fizzBuzz(int number) {
    if (number % 3 == 0) {
      if (number % 5 == 0) {
        return "fizzbuzz";
      } else {
        return "fizz";
      }
    } else if (number % 5 == 0) {
      return "buzz";
    }
    return String.valueOf(number);
  }
}
