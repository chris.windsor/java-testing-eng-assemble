package personal.areyas.chris;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class FunParamsTest {

  @Test
//  @Parameters({"3, fizz", "10, buzz", "1, 1"})
  @Parameters(method = "fizzBuzzParams")
  public void fizzBuzzTest(int input, String expected) {
    assertThat(Fun.fizzBuzz(input), equalTo(expected));
  }

  public Object[] fizzBuzzParams() {
    return new Object[] {
            new Object[] {"3", "fizz"},
            new Object[] {"10", "buzz"},
            new Object[] {"1", "1"}
            , new Object[] {"15", "fizzbuzz"}
    };
  }
}
