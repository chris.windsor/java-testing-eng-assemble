package personal.areyas.chris;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class FunTest {

  @Test
  public void addOneTest() {
    assertThat(Fun.addOne(1), equalTo(2));
  }

  @Test
  public void fizzBuzzDivisibleBy3Test() {
    assertThat(Fun.fizzBuzz(3), equalTo("fizz"));
  }

  @Test
  public void fizzBuzzDivisibleBy5Test() {
    assertThat(Fun.fizzBuzz(10), equalTo("buzz"));
  }

  @Test
  public void fizzBuzzNotDivisibleBy3Or5Test() {
    assertThat(Fun.fizzBuzz(1), equalTo("1"));
  }

//  @Test
//  public void fizzBuzzDivisibleBy3And5Test() {
//    assertThat(Fun.fizzBuzz(15), equalTo("fizzbuzz"));
//  }
}